# !/usr/bin/Python

# website: https://pythonprogramminglanguage.com/modules

'''Create your own module
First create a Python file with a function.
We call this file <file-name>.py and we have one function'''

def ownModule():
    print('this is own module')
